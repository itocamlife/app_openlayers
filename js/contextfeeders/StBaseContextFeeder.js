/**
	ベースコンテキストフィーダークラス
*/
class StBaseContextFeeder {
	constructor(name) {
		this.name = name;
		this.context = [];
	}
	GetContext(conditions){
		var context = [];
		
		if(condistions && condistions.locations){
			for(let obj of this.context) {
				if(	  obj.locations.latlng.lat == condistions.locations.latlng.lat
				   && obj.locations.latlng.lng == condistions.locations.latlng.lng
				){
					context.push(obj);
				}
			}
		}
		return context;
	}
}
