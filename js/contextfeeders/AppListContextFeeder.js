/**
	AppListコンテキストフィーダークラス
*/

class AppListContextFeeder extends StBaseContextFeeder {
	constructor() {
		super("AppList");
	}

	// アプリ一覧の取得
	GetContext(conditions){
		var appList = document.getElementById("sortable").children;

		if (appList.length == g_config.app_list.length) {
			// 画面上のアプリ一覧に全てのアプリが展開済の場合は、
			// DOMからアプリ一覧を生成する（画面上での並び順が反映された状態でアプリ一覧を取得）
			var context = [];
			for(let app of appList) {
				var appNum = Number(app.children[1].value);
				context.push(g_config.app_list[appNum]);
			}
			return context;
		} else {
			// 画面上のアプリ一覧に未展開のアプリがある場合は、
			// アプリ定義情報をそのまま返す
			return g_config.app_list;
		}
	}

}
