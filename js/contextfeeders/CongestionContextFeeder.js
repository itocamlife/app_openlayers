/**
	混雑コンテキストフィーダークラス
*/
class CongestionContextFeeder extends StBaseContextFeeder {
	constructor() {
		super("Congestion");
	}

	GetContext(conditions){
		if(!conditions) return null;
		var poiCode;
		if(conditions.id){
			poiCode = conditions.id;
		}
		else if(conditions.locations){
			// 伊都キャンパスの全POI取得
			var poiLocsAll = g_myPoiController.getPoiLocs();
			// lat, lngでpoi特定
			for ( var poi of poiLocsAll ) {
				if(	  poi.locations.latlng.lat==conditions.locations.latlng.lat
				   && poi.locations.latlng.lng==conditions.locations.latlng.lng
				){
					poiCode = poi.id;
					break;
				}
			}
		}
		if(!poiCode) return null;
		return this.getCongestion(poiCode);
	}

	// 混雑情報を返す
	getCongestion(poiCode){
		var averageCongestion = g_myPoiController.calculateCongestion(poiCode);
		var averageCongestion_trn = g_myPoiController.calculateCongestion(poiCode, 2);
		var congistion = {
			averageCongestion: averageCongestion,
			starsStr: this.makeCongestionStars(averageCongestion),
			rankColor: this.chooseStarColor(averageCongestion),
			averageCongestion_trn: averageCongestion_trn,
			starsStr_trn: this.makeCongestionStars(averageCongestion_trn),
			rankColor_trn: this.chooseStarColor(averageCongestion_trn),
		};
		
		return congistion;
	}

	makeCongestionStars(ave) {
		//混雑度状況の星印を作る
		var str = "",
				starCount = 0;

		if(ave) {
			//四捨五入して,1,2,3のいずれかにする
			starCount = Math.round(ave);
			//値に応じた星の数を表示する
			for(let i = 1; i <= 3; i++) {
				if(i <= starCount) {
					str += "★";
				} else {
					str += "☆";
				}
			}
		} else {
			str = "-";
		}
		return str;
	}

	chooseStarColor(ave) {
		//3:赤,2:黃,1,0:緑
		var color = "black";

		if(ave <= 3 && ave > 2) color = "#c0504d";
		else if(ave <= 2 && ave > 1) color = "#f79646";
		else color = "#9bbb59";

		return color;

	}
}
