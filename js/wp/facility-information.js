var FacilityInformation = function() {
  /*
    poicodeに対応する施設情報を取得する
    poiCode: {}
  */
  this.facilityData = {};
  this.emptyData = {
    "image_src": "",
    "text": ""
  };
}

FacilityInformation.prototype.loadData = function(apiRoot, poiCode) {
  /*
  poicodeに対応する施設情報を取得する
  もし、すでにpoicodeに対応するデータがあるなら何もしない
  */
  const GET_DATA_URL = apiRoot;
  var that = this;

  if(that.hasData(poiCode)) return (that.getData(poiCode));

  $.ajax({
      type: "GET",
      url: GET_DATA_URL + poiCode,
      dataType: "json",
      async: false,
      success: function(data) {
//        console.log("loadFacilityData:",data);
        that.addData(poiCode, data);
      },
      error: function(data) {
      }
  });
}

FacilityInformation.prototype.hasData = function(poiCode) {

  if(poiCode in this.facilityData) return true;
  return false;

}

FacilityInformation.prototype.getData = function(poiCode) {
  return this.facilityData[poiCode];
}

FacilityInformation.prototype.addData = function(poiCode, data) {
  var contents = "",
      imageSrc = "",
      text = "",
      tags = [];

  if(! data || !data["contents"]) return;

  contents = data["contents"];
  if(contents["images"]) imageSrc = contents["images"][0]["original"];
  if(contents["article"]) {
	  if(contents["article"]["text"]) {
		text = contents["article"]["text"].substr(1, 30) + "...";
	  }
   }

  if (data.hasOwnProperty("tags")) {
    if (data["tags"].hasOwnProperty("wp_tags")) {
      tags = data["tags"]["wp_tags"];
    }
  }

  this.facilityData[poiCode] = {
    "image_src": imageSrc,
    "text": text,
    "tags": tags
  };
}
