/*
  POIの評価(混雑度など)を行う
*/

var PoiEvaluator = function(){
  /*
  poi評価情報

  */
  this.poiEvaluations = [];

}


PoiEvaluator.prototype.searchPoiEvaluations = function(apiRoot){

  /*
    POI情報に付随する評価情報の取得(混雑度など)
  */

  const GET_EVALUATE_URL = apiRoot + "/IGB/restapi/IGBPoiEvaluation.php?callback=?";
  var that = this;

  return new Promise(function(resolve, reject){
    $.ajax({
      type: "GET",
      url: GET_EVALUATE_URL,
      dataType: "jsonp",
      timeout: 3*1000,
      success: function(data) {
        console.log("evaluations:",data);
        for(let obj in data){
          that.addPoiEvaluation(data[obj]);
        }
        console.log("success_getPoiEvaluations:",that.poiEvaluations);
        resolve();
      },
      error: function(data) {
        reject();
      }
    })
  });
}

PoiEvaluator.prototype.addPoiEvaluation = function(evaluation){
  /*
    Poi評価情報を追加する
  */
  this.poiEvaluations.push(evaluation);
}

PoiEvaluator.prototype.hasEvaluation = function(poicode){
  /*
    目的のPoiの評価情報が存在するか判定
    存在するなら該当するPOI評価リストを返す
    存在しないならFalseを返す
  */
  var evaluations = this.poiEvaluations;
  var targetEvaluations = [];

  for(let obj of evaluations) {
    if(poicode === obj["poi_code"]) targetEvaluations.push(obj);
  }

  if(targetEvaluations.length != 0) return targetEvaluations;
  else return false;

}



PoiEvaluator.prototype.calculateCongestion = function(poicode){
  /*
    Poiの混雑度を計算する
    poicode: String
    -> float:平均混雑状況
  */
  var targetEvaluations = this.hasEvaluation(poicode);
  var congestionRankSum = 0;
  var congestionAverageRank;

  if(!targetEvaluations) return "-"; //該当するPOI評価情報がないなら未評価

  for(let obj of targetEvaluations) {
    let rank = parseInt(obj["rank"]);
    console.log("rank:",rank);
    if(Number.isFinite(rank)) congestionRankSum += rank;
  }
  //小数点第２位を四捨五入
  congestionAverageRank = Math.round(congestionRankSum * 10 / targetEvaluations.length) / 10;

  return congestionAverageRank;

}
