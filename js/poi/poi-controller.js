/*
周辺のPOI情報を取得する
*/

var PoiController = function(){
  this.poiLocs = [];
  this.poiEvaluations = {};
  this.api_root_v2 = '';
  this.tagList = {};
  this.myFacilityInformation = new FacilityInformation();
}

// API設定の取得
PoiController.prototype.setConfig = function(config){
  this.api_root_v2 = config.api_root_v2;
}

PoiController.prototype.searchPoiLocs = function(apiRoot){
  /*
    キャンパス周辺のGPS範囲を渡し、その周辺のPOI情報を取得する
  */
  //POI取得先URL
  const GET_POI_URL = apiRoot + "find";
  //キャンパス周辺のGPS範囲
  const NORTH_EAST_LAT = 33.607797000000000;
  const SOUTH_WEST_LAT = 33.585611000000000;
  const NORTH_EAST_LNG = 130.2383360000000;
  const SOUTH_WEST_LNG = 130.2011050000000;

//マップの中心GPS座標
  const MAP_CENTER_LAT = 33.5985408303938;
  const MAP_CENTER_LNG = 130.21669284217387;

  var that = this;
  var param = {
            "locations.latlng.lat" : { '$gte': SOUTH_WEST_LAT, '$lte': NORTH_EAST_LAT },
            "locations.latlng.lng" : { '$gte': SOUTH_WEST_LNG, '$lte': NORTH_EAST_LNG },
            "statuses.published" : { $in: [ "1", true ] }
  };

  return new Promise(function(resolve, reject) {
    superagent
      .post (GET_POI_URL)
      .send ( param || {} )
      .end ( ( err, res ) => {
        if (err) {
          console.log("searchPois:NG!");
          console.log(err);
          reject();
        } else {
          console.log("success_search_poi:", res.body);
          that.setPoiLocs(res.body);

          resolve();
      }
    });
  });
}

PoiController.prototype.choosePoiByCategory = function(poiLocs, categoryNames){
  /*
    カテゴリー名で絞ったPOI情報を返す
    poiLocs POI情報(Array), categoryName カテゴリー名(String)
  */
  var selectedPoiLocs = [];

  for(let obj of poiLocs) {
    let categoryArr = obj["tags"]["wp_categories"];
    for(let category of categoryArr) {
      //該当するカテゴリーを一つでも含んでいる場合、リストに追加して次のpoiを参照する
      if(categoryNames.indexOf(category) >= 0) {
        selectedPoiLocs.push(obj);
        break;
      }
    }
  }

  return selectedPoiLocs;

}
PoiController.prototype.choosePoiByKeywords = function(poiLocs, keywords){
  /*
    キーワード名で絞ったPOI情報を返す
    タイトルをキーワードで絞り込む
    キーワードが複数の時はアンドで処理する
    poiLocs POI情報(Array), keywords キーワード群(Array)
    何もないなら、全てのPOI情報を返す
  */
  var selectedPoiLocs = [];
  var isContainAllKeywords = function(title, text, tags, keywords) {
    //キーワードを全て含んでいるかを判定する
    //全て含んでいる場合 true, 一つでも含んでいない場合 false
    console.log("poi-cont:",title, text, tags,keywords);
    for(let word of keywords) {
      if ( title.indexOf(word) == -1
          && text.indexOf(word) == -1
          && tags.join(',').indexOf(word) == -1 ) {
        return false;
      }
    }
    return true;
  }

  for(let obj of poiLocs) {
//    console.log('POI SEARCH',obj);
    let title = obj["name"];
    let text = obj["contents"]["article"]["text"] || "";
    let tags = obj["tags"]["wp_tags"];

    if(isContainAllKeywords(title, text, tags, keywords)) selectedPoiLocs.push(obj);
  }

  return selectedPoiLocs;
}

PoiController.prototype.getInfoWindowKeys = function(poiCode, poiLocs=null) {
  var matchedPoiLoc = {},
      windowObj = {
        "url": "",
        "dest_name": "",
        "text": "",
        "imageSrc": "",
        "categories": "",
        "tags": "",
        "evaluations": [],
        "child_poi_ids": []
      };

  if (!poiLocs) {
    poiLocs = this.poiLocs;
  }

  for(let poi of poiLocs) {
    if(poiCode === poi["id"]) matchedPoiLoc = poi;
  }
  //乗換案内のパスを自動変換
  if(matchedPoiLoc["contents"]["article"]["web"].indexOf("http://patrash.ait.kyushu-u.ac.jp:8080") != -1) {
    windowObj["url"] = matchedPoiLoc["contents"]["article"]["web"].replace(/patrash/, "ito").replace(/http/, "https");
  } else {
    windowObj["url"] = matchedPoiLoc["contents"]["article"]["web"];
  }

  windowObj["dest_name"] = matchedPoiLoc["name"];

  var text = matchedPoiLoc["contents"]["article"]["text"];
  windowObj["text"] = text ? text.substr(1, 30) + "..." : "";

  windowObj["imageSrc"] = matchedPoiLoc["contents"]["images"][0]["original"];
  windowObj["categories"] = matchedPoiLoc["tags"]["wp_categories"].join(", ");

  var tags_except_lang = matchedPoiLoc["tags"]["wp_tags"].filter(tag => {
    return !tag.startsWith("lang:");
  });

  windowObj["tags"] = tags_except_lang.join(", ");

  var evaluations = matchedPoiLoc["evaluations"];

  if ( evaluations ) {
    windowObj["evaluations"] = evaluations;
  }

  var child_poi_ids = matchedPoiLoc["child_poi_ids"];

  if ( child_poi_ids ) {
    windowObj["child_poi_ids"] = child_poi_ids;
  }

  return windowObj;

}

PoiController.prototype.getPoiLocs = function(){
  /*
    POIリストを返す
  */
  return this.poiLocs;
}

PoiController.prototype.setPoiLocs = function(poi_arr){
  /*
    取得したPOI配列をセットする
  */
  this.poiLocs = poi_arr;

  // 評価が存在するものだけ集める
  this.poiEvaluations = {};

  for ( var poi of poi_arr ) {
    if ( poi.hasOwnProperty('evaluations') ) {
      this.poiEvaluations[poi.id] = poi['evaluations'];
    }
  }
}

PoiController.prototype.addPoiLocs = function(poi){
  /*
    部分的に取得したPOI情報を追加する
  */
  this.poiLocs.push(poi);
}

PoiController.prototype.removeAllPoiLocs = function(){
  /*
    POI情報を全削除する
  */
  this.poiLocs = [];
}

PoiController.prototype.calculateCongestion = function(poicode, type = 1){
  /*
    Poiの評価値を計算する
    poicode: String
    type: int (1:混雑度合い、2:乗換案内使いやすさ)
    -> float:平均混雑状況
  */
 if ( !this.poiEvaluations.hasOwnProperty(poicode) ) return "-"; //該当するPOI評価情報がないなら未評価
 if ( !this.poiEvaluations[poicode].length ) return "-";

  var targetEvaluations = this.poiEvaluations[poicode];
  var targetEvaluations_cnt = 0;
  var congestionRankSum = 0;
  var congestionAverageRank;
  var rankExists = 0;

  for(let obj of targetEvaluations) {
    if ( obj["type"] === type ) {
      rankExists = 1;
      let rank = parseInt(obj["rank"]);
      if(Number.isFinite(rank)) {
        congestionRankSum += rank;
        targetEvaluations_cnt++;
      }
    }
  }

  if ( !rankExists ) return "-";

  //小数点第２位を四捨五入
  congestionAverageRank = Math.round(congestionRankSum * 10 / targetEvaluations_cnt) / 10;

  return congestionAverageRank;

}
