/*
公式サイトからのイベント及びアプリユーザの個人企画イベントの管理
*/

var EventController = function(){
  this.officialEventUrl = "";
  this.personalEventUrl = "";
  this.events = {
    "official": [],
    "personal": []
  };
  this.errorMessage = [];
}

EventController.prototype.setConfig = function(config){
  this.officialEventUrl = config.officialEventUrl;
  this.personalEventUrl = config.personalEventUrl;
}

EventController.prototype.fetchOfficialEvent = function(){
  /*
   * まだ終わっていない公式イベントを取得する
   *
   */
  var nowDate = new Date();
  var that = this;

  return new Promise(function(resolve,reject){

    $.ajax({
      url: that.officialEventUrl,
      type: "GET",
      timeout: 5*1000
    }).done(function(result){
      result = JSON.parse(result);
      var eventArr = [];
      for(var i = 0; i < result.length; i++){
        var lineArr = result[i].split("@");
        var eventData = {
          "title": lineArr[0],
          "page_link": lineArr[1],
          "image_src": lineArr[2],
          "position": lineArr[3],
          "start_date": lineArr[4],
          "end_date": lineArr[5]
        };
        if(lineArr.length != 6){
          continue;
        }
        var endTime = new Date(eventData["end_date"]).getTime();
        var nowTime = nowDate.getTime();
        if(endTime > nowTime){
          eventArr.push(eventData);
        }
      }
      that.setEvent("official", eventArr);
      resolve();
    }).fail(function(jqXHR, textStatus, errorThrown){
      console.error("failed to load official events");
      console.log("XMLHttpRequest : " + XMLHttpRequest.status);
      console.log("textStatus     : " + textStatus);
      console.log("errorThrown    : " + errorThrown.message);
      that.errorMessage.push('公式イベント情報の取得に失敗しました');
      resolve();
    });

  });
}

EventController.prototype.fetchPersonalEvent = function(){
  /*
   * 個人企画イベントの取得
   */
  var nowDate = new Date();
  var that = this;

  return new Promise(function(resolve, reject){
    $.ajax({
      url: that.personalEventUrl,
      type: "GET",
      timeout: 5*1000
    }).done(function(result){
      result = JSON.parse(result);
      console.log(result);
      var eventArr = [];
      for(var i = 0; i < result.length; i++){
        var lineArr = result[i].split("@@");
        var eventData = {
          "event_id": lineArr[0],
          "title": lineArr[1],
          "position": lineArr[2],
          "config_position": lineArr[3],
          "start_date": lineArr[4],
          "end_date": lineArr[5],
          "content": lineArr[6],
          "user_name": lineArr[7],
          "user_addr": lineArr[10]
        };
        var endTime = new Date(eventData["end_date"]).getTime();
        var nowTime = nowDate.getTime();
        if(endTime > nowTime){
          eventArr.push(eventData);
        }
      }
      that.setEvent("personal", eventArr);
      resolve();
    }).fail(function(jqXHR, textStatus, errorThrown){
      console.error("failed to load personal events");
      console.log("XMLHttpRequest : " + XMLHttpRequest.status);
      console.log("textStatus     : " + textStatus);
      console.log("errorThrown    : " + errorThrown.message);
      console.error(errorThrown);
      that.errorMessage.push('個人企画イベント情報の取得に失敗しました');
      resolve();
    });
  });
}

EventController.prototype.dispAlert = function(){
  if ( this.errorMessage.length ) {
    alert(this.errorMessage.join('\n'));
  }
}

EventController.prototype.getEvent = function(){
  return this.events;
}

EventController.prototype.setEvent = function(mode, eventArr){
  console.log(mode,":",eventArr);
  this.events[mode] = eventArr;
}

EventController.prototype.matchedPersonalEvent = function(eventId) {
  console.log("eventId:", eventId);
  for(let obj of this.events["personal"]) {
    if(obj["event_id"] === eventId) return obj;
  }
  return false;
}

EventController.prototype.matchedByPositionName = function(positionName) {
  var selectedEvents = {
    "official": [],
    "personal": []
  };
  for(let obj of this.events["official"]) {
    console.log(obj);
    if(positionName === obj["position"]) selectedEvents["official"].push(obj);
  }

  for(let obj of this.events["personal"]) {
    console.log(obj);
    if(positionName === obj["position"]) selectedEvents["personal"].push(obj);
  }

  return selectedEvents;
}

EventController.prototype.countEvent = function(destName){
  var eventCount = 0;
  var that = this;
  eventCount += count("official");
  eventCount += count("personal");

  return eventCount;

  function count(mode) {
    var c = 0;
    for(let obj of that.events[mode]){
      var position = obj["position"];
      if(destName === position) c++;
    }
    return c;
  }
}
