//controll event list DOM
var EventListUI = function(){

}

EventListUI.prototype.showOfficialEventList = function(positionName) {

  var officialEventList = g_myEventController.getEvent()["official"];
  var that = this;

  if(positionName) officialEventList = g_myEventController.matchedByPositionName(positionName)["official"];
  console.log(officialEventList);
  $("#official_event_list").empty();

  officialEventList.forEach((eventData) => {
    var eventDom = $("#official_event_list");
    var newEvent = $(`
      <li class="event">
        <button>
          <input type="hidden" id="page_src" value="${eventData["page_link"]}" />
          <div class="image">
            <img class="event_img" src="${eventData["image_src"]}">
            </img>
          </div>
          <div class="event_content">
            <h2 class="event_title">${eventData["title"]}</h2>
            <h2 class="event_position">${eventData["position"]}</h2>
            <h2 class="event_date">${eventData["start_date"]} ~ ${eventData["end_date"]}</h2>
          </div>
        </button>
      </li>
    `);

    newEvent.on("click", function() {
      that.empty();
      var url = $(this).find("#page_src").val();

      $("#official_event_frame").attr("src", url);
      switchTab("event_page");
    });

    eventDom.append(newEvent);
  });
}

EventListUI.prototype.showPersonalEventList = function(positionName) {
  var personalEventList = g_myEventController.getEvent()["personal"];
  var that = this;
  if(positionName) personalEventList = g_myEventController.matchedByPositionName(positionName)["personal"];
  console.log(personalEventList);
  $("#personal_event_list").empty();

  personalEventList.forEach((eventData) => {
    var eventDom = $("#personal_event_list");
    var newEvent = $(`
      <li class="personal_event">
        <button>
          <input id="event_id" value="${eventData["event_id"]}" type="hidden"></input>
          <div class="event_content">
            <h2 class="event_title">
              ${eventData["title"]}
            </h2>
            <h2 class="event_position">
              ${eventData["position"]}
            </h2>
            <h2 class="event_date">
              ${eventData["start_date"]} ~ ${eventData["end_date"]}
            </h2>
          </div>
        </button>
      </li>
      `);
    newEvent.on("click", function() {
      that.empty();
      var eventId = $(this).find("#event_id").val(),
          personalEvent = g_myEventController.matchedPersonalEvent(eventId),
          personalEventFrame = $("#personal_event_frame"),
          eventContent = "";

      if(!personalEvent) alert("Sorry,failed to load the event!");

      console.log(personalEvent);
      eventContent = personalEvent["content"].replace(/\r\n|\n/,"<br/>");

      var personalEventDom = $(`
          <h1 id="personal_config_title">${personalEvent["title"]}</h1>
          <hr />
          <h2 id="personal_config_content">${eventContent}</h2>
          <hr />
          <table border=1>
            <tr><th>開催日時</th><td id="personal_config_date"><h2>${personalEvent["start_date"]} ~ ${personalEvent["end_date"]}</h2></td></tr>
            <tr><th>場所</th><td id="personal_config_position"><h2>${personalEvent["position"]} : ${personalEvent["config_position"]}</h2></td></tr>
            <tr><th>開催者情報</ht><td id="personal_config_owner"><h2>${personalEvent["user_name"]} : ${personalEvent["user_addr"]}</h2><td></tr>
          </table>
        `);

      personalEventFrame.append(personalEventDom);
      switchTab("event_page");
    });

    eventDom.append(newEvent);
  });
}

EventListUI.prototype.emptyList = function() {
  $("#official_event_list").empty();
  $("#personal_event_list").empty();
};
EventListUI.prototype.empty = function() {
  $("#official_event_frame").attr("src", "");
  $("#personal_event_frame").empty();
}
