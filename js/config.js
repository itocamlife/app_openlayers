// POI取得用REST-APIのルートパス
export const api_root_v2= "https://itocamlife.go1.jp:53002/pois/";

// 公式イベント取得用REST-APIのパス
export const officialEventUrl = "https://ito.ait.kyushu-u.ac.jp/itocam_event/send_event_inf_2.php";

// 個人イベント取得用REST-APIのパス
export const personalEventUrl = "https://ito.ait.kyushu-u.ac.jp/itocam_event/send_personal_event_inf.php";

// 地図の中心位置の初期値
export const initial_location = {
      lat: 33.596504,
      lng: 130.218759,
      zoom: 15
};

/**
	コンテキストレイヤー定義
*/
export const ctx_layer_congestion = layer("ctx_congestion");

export const ctx_layer_poi_buslocation = layer("ctx_poi_buslocation");

export const ctx_layer_lang_ja = layer("ctx_lang_ja");
export const ctx_layer_lang_en = layer("ctx_lang_en");

export const app_layer_education = layer("app_education");
export const app_layer_cafeteria = layer("app_cafeteria");
export const app_layer_event = layer("app_event");
export const app_layer_busstation = layer("app_busstation");

/**
    アプリ定義

        Name: POI取得対象のカテゴリ名
        checkId: アプリ選択欄のチェックボックスのid属性に付与する一意な値
        contextLayers: アプリに対して適用するコンテキストレイヤーを配列形式で指定
        pinImage: マップ上に表示するアイコン画像のパス
*/
export const app_list = [
    {
        Name: "教育施設",
        checkId: "layer1",
        contextLayers: [],
        pinImage: "img/marker-red.png",
    },
    {
        Name: "食堂情報",
        checkId: "layer2",
        contextLayers: [ctx_layer_congestion],
        pinImage: "img/marker-blue.png",
    },
    {
        Name: "イベント情報",
        checkId: "layer3",
        contextLayers: [app_layer_event],
        pinImage: "img/marker-star.png",
    },
    {
        Name: "乗換案内",
        checkId: "layer4",
        contextLayers: [app_layer_busstation],
        pinImage: "img/marker-busstop.png",
    },
/*    {
        Name: "バス現在位置",
        checkId: "layer5",
        contextLayers: [ctx_layer_poi_buslocation],
        pinImage: "img/marker-buslocation.png",
    },*/
];

export const lang_list = {
    ja:{
        Name: "日本語",
        radioId: "ja",
        contextLayer: ctx_layer_lang_ja,
        checked: true,
    },
    en:{
        Name: "English",
        radioId: "en",
        contextLayer: ctx_layer_lang_en,
    },
}
