/*
// 教育施設のレイヤーに描画するPOIリスト
var layer1_pois = [
	{lng: 130.220817, lat: 33.596478, content: '九州大学ウエスト１号館' },
	{lng: 130.224944, lat: 33.597788, content: '九州大学センター３号館' },
	{lng: 130.22268, lat: 33.59533, content: '総合体育館' },
]

// 食堂情報のレイヤーに描画するPOIリスト
var layer2_pois = [
	{lng: 130.223735, lat: 33.596966, content: 'カフェテリア　『Qasis』' },
	{lng: 130.221024, lat: 33.596051, content: 'GARDEN KITCHEN(理学部食堂)' },
	{lng: 130.216432, lat: 33.596837, content: '九大あかでみっく らんたん' },
]
*****/
import * as config from "./config.js";

// 各種設定を他のクラス等からも参照できるようにグローバル変数にセット
window.g_config = config;

function onInitShow() {
	// 地図のLayerをOpen Street Map (OSM)を利用して作成
	var osmLayer = new ol.layer.Tile({
		source: new ol.source.OSM()
	});

	//吹き出し用オーバレイ準備
	var container = $('#popup')[0];
	var closer = $('#popup-closer')[0];
	var popoverlay = new ol.Overlay({
		id: 'popoverlay',
		positioning: 'bottom-center',
		element: container,
		autoPan: true,
		autoPanAnimation: {
			duration: 250
		}
	});

	popoverlay.setOffset([0, -40]);

	//吹き出しの閉じるボタン
	closer.onclick = function() {
		popoverlay.setPosition(undefined);
		closer.blur();
		return false;
	};

	// マップ表示
	g_map = new ol.Map({
		layers: [ osmLayer ], // 表示するLayerの定義
		target: document.getElementById('mapResult'),	// 表示先のdivタグを指定
		overlays: [popoverlay], 						// 表示するOverlayの定義
		view: new ol.View({
			center: convertCoordinate(g_config.initial_location.lng, g_config.initial_location.lat),	// 中心座標の初期値
			zoom: g_config.initial_location.zoom		// 拡大率の初期値
		})
	});

	g_map.on('click', function(evt) {
		console.log("g_map.on(click):"+evt.pixel);
	// 地図上でクリックした位置にあるPOIマーカーを取得
		var onLayers = [];
		var features = [];
		g_map.forEachFeatureAtPixel(evt.pixel,function(feature, layer) {
				features.push(feature);
				onLayers.push(layer);
			});

		// クリック位置にPOIマーカーがあれば吹き出しを表示
		if (features.length>0) {
			var feature = features[0];
			showPopup(onLayers[0].get("StLayer"), feature);
		}
		else {
			//吹き出しを閉じる
			closer.onclick();
		}
	});
	// change mouse cursor when over marker
	$(g_map.getViewport()).on('mousemove', function(e) {
		var pixel = g_map.getEventPixel(e.originalEvent);
		var hit = g_map.forEachFeatureAtPixel(pixel, function(feature, layer) {
			return true;
		});
		if (hit) {
			g_map.getTarget().style.cursor = 'pointer';
		} else {
			g_map.getTarget().style.cursor = '';
		}
	});

	// アプリ選択ボタンクリック時の追加処理
	$('#application-Select-Button, #Language-Select-Button').click( function(){
		//吹き出しを閉じる
		closer.onclick();
		return false;
	});

	// アプリケーションリストコンテキストフィーダ登録
	gContextFeederStack.AddContextFeeder(new AppListContextFeeder());

	// 混雑コンテキストフィーダ登録
	gContextFeederStack.AddContextFeeder(new CongestionContextFeeder());

	// アプリケーション生成
	// index.htmlのsortableリストを取得
//	var context = gContextFeederStack.GetContext(null, function(name){ return ("AppList"==name);});
//	for(let app of context["AppList"]) {
	var appList = gContextFeederStack.getFeeder("AppList").GetContext(null);
	var appNum = 0;

	// 各アプリに対して、クラスのインスタンス生成とアプリ一覧への追加を行う
	for(let app of appList) {
		console.log(app.Name);
		appNum++;
		var appName = app.Name;
		var checkId = app.checkId;
		var pinImage = app.pinImage;
		var contextLayers = app.contextLayers;
		g_appStack[appName] = new StBaseApp();
		g_appStack[appName].init(appName, pinImage, contextLayers);

		// アプリ一覧への追加
		$("#sortable").append(`
			<li id="app${appNum}">
				<label class="classlayer" for="${checkId}">
					<input type="checkbox"
					       id="${checkId}"
						   class="checkbox-layer" checked>${appName}
				</label>
				<input type="hidden" name="appNum" value="${appNum-1}">
			</li>
		`);
	}

	for (var lang_id in g_config.lang_list) {
		// 言語一覧への追加
		var lang_item = g_config.lang_list[lang_id];

		var checked = lang_item.checked ? "checked" : "";

		$("#language").append(`
			<li id="lang_${lang_id}">
				<label class="classlayer" for="${lang_item.radioId}">
					<input type="radio"
						   id="${lang_item.radioId}"
						   name="grp_language"
						   value="${lang_id}"
						   class="checkbox-layer" ${checked}>${lang_item.Name}
				</label>
			</li>
		`);
	}

	$(document).on("change", "[name='grp_language']", () => {
		var lang_id = $("[name='grp_language']:checked").val();
		var lang_item = g_config.lang_list[lang_id];
		var ctx_layer_lang = lang_item.contextLayer

		for (var app_name in g_appStack) {
			var app = g_appStack[app_name];
			for (lang_id in g_config.lang_list) {
				lang_item = g_config.lang_list[lang_id];
				app.removeContextLayer(lang_item.contextLayer);
			}
			/* 言語コンテキストレイヤーを、アプリのコンテキストレイヤー配列の最後に追加する。
			   同じメソッド（今回はupdateメソッド）が複数のコンテキストレイヤーに定義されている場合、
			   最後のコンテキストレイヤーのメソッドしか呼び出されないが、
			   メソッドの中でproceed()を呼び出すと、上位コンテキストレイヤーの
			   メソッドが実行される。今回は、言語のupdate→混雑のupdateのように、
			   言語のupdateを最初に実行したいので、言語コンテキストレイヤーは
			   必ずアプリのコンテキストレイヤー配列の最後に追加する必要がある。
			*/
			app.addContextLayer(ctx_layer_lang);
			withLayers(app.layer.contextLayers, () => {
				app.layer.update();
			});
		}
	});
}

$(document).ready( function() {
// mod -s 20181031 motoda test
//	//コンフィグjsonを読み取る
//	$.getJSON('config.json', function(config){
//		console.log(config);
//		console.log(config.api_root_v2);
//	});
//	$.get("config.json", function(data){
//		var config = $.parseJSON(data);
//		console.log(config);
//		console.log(config.api_root_v2);
//	});
	g_myPoiController = new PoiController();
	g_myEventController = new EventController();
	g_myEventController.setConfig(g_config),
	myEventListUI = new EventListUI();
//	g_myPoiController.searchPoiLocs(config.api_root_v2);
//	g_myPoiController.searchPoiLocs("https://kataribe.cloud:53001/pois/");
		//先に読み込んでおきたい情報を非同期で全部取得
		var initPromiseArray = [
//			g_myPoiController.setConfig(config),
//			g_myPoiController.searchPoiLocs(config.api_root_v2),
			g_myPoiController.searchPoiLocs(g_config.api_root_v2),
//			g_myPoiController.searchPoiLocs_local("https://kataribe.cloud:53001/pois/"),
			g_myEventController.fetchOfficialEvent(),
			g_myEventController.fetchPersonalEvent()
		];

		Promise.all(initPromiseArray).then(function(){
			//終了時にマップの描画などを行う
			onInitShow();
		});
// mod -e 20181031 motoda test
/*	var mapOption = {
		mapID: 'mapResult',
		clickable: false,
		zoom: MAP_ZOOM_DEFAULT_RESULT,
		zoom_min: MAP_ZOOM_MIN_RESULT,
	};

	g_map_result = new extendMap(mapOption);
	g_map_result.createMap();
	loadResultMap();
*/
//20181023 mishima start
//「アプリ選択」の一番目のアプリ名を初期表示する
	$("#applicationInfo").html(g_config.app_list[0].Name);
//20181023 mishima end

//20181018 mishima
	$('#application-Select-Button, #Language-Select-Button').click( function(){
		//「アプリ選択」ボタンを押下されて、すでにアプリ選択のメニューが開いていた場合の処理
		if($(this).hasClass("open")) {
			$(this).removeClass("open");
			$(this).parent().next().hide();
		//「アプリ選択」ボタンを押下されて、アプリ選択のメニューが開いていない場合の処理
		}else {
			var menu = $(this).parent().next().show().position( {
				my: "center top",
				at: "center bottom",
				of: this
			} );
			$(this).addClass("open");

			var hide_menu_button;
			if ($(this).prop("id") == "application-Select-Button") {
				hide_menu_button = $("#Language-Select-Button");
			} else {
				hide_menu_button = $("#application-Select-Button");
			}
			$(hide_menu_button).removeClass("open");
			$(hide_menu_button).parent().next().hide();
		}

		var that = this

		//すでにアプリ選択のメニューが開いた状態でメニュー以外の箇所を押下した場合の処理
		$(".ol-viewport").one( "click", function() {
			$(that).removeClass("open");
			$(that).parent().next().hide();
		});
		return false;
	});

	//ソート設定
	$('.sortable-list').sortable({
		//ソート処理終了時にヘッダーのアプリ名を「アプリ選択」一番目のアプリ名に更新する
		stop:function(){
			var appList = document.getElementById("sortable").children[0].children;
			$("#applicationInfo").html(appList[0].innerText);
			appOrderChange();
		}
	});
//20181018 mishima
});
