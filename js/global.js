/**
	index.js以外からも使用する変数・関数群
*/

class ContextFeederStack {
	constructor(){
		this.feederStack = {};
	}
	AddContextFeeder(contextFeeder){
		this.feederStack[contextFeeder.name] = contextFeeder;
	}
/*
	//コンテキストを取得
	GetContext(lat, lng, stime, etime, name){
		var context = {};
		if(name){
			context[name] = this.feederStack[name].GetContext(lat, lng, stime, etime);
		}
		else {
			for(name in this.feederStack) {
				context[name] = this.feederStack[name].GetContext(lat, lng, stime, etime);
			}
		}
		return context;
	}
*/
	//コンテキストを取得
	// 引数: conditions コンテキスト検索条件を連想配列で指定
	//       コンテキストフィーダー側で必要な条件がセットされているかチェックする
	//     : filter コンテキストフィーダーを絞り込むフィルタ
	// 戻り値:フィーダー名をキーとした連想配列
	// 例1)
	//  var condistion = { locations.latlng.lat:33.596084 , locations.latlng.lng:130.22142 };
	//	var context = gContextFeederStack.GetContext(condistion);
	// 例2)フィルターを使用して対象のコンテキストフィーダーを絞り込む場合 -->
	//  var condistion = { locations.latlng.lat:33.596084 , locations.latlng.lng:130.22142 };
	//	var context = gContextFeederStack.GetContext(condistion, function(name){ return (name=="Congestion");});
	//
	GetContext(conditions, filter){
		var context = {};
		for(name in this.feederStack) {
			if((filter) && !filter(name)) continue;
			var ret = this.feederStack[name].GetContext(conditions);
			if(null!=ret){
				context[name] = ret;
			}
		}
		return context;
	}

	// 指定した名称のフィーダを返す
	getFeeder(name){
		return this.feederStack[name];
	}
}

var g_map;
var g_myPoiController;
var g_myEventController;
var g_appStack = {};	// アプリケーションスタック
var gContextFeederStack = new ContextFeederStack();	// コンテキストフィーダースタック
//UI DOM Instance
var myEventListUI;

// 座標の変換
function convertCoordinate(longitude, latitude) {
	return ol.proj.transform([longitude, latitude], "EPSG:4326","EPSG:900913");
}

function appOrderChange(){
	for(key in g_appStack){
		withLayers(g_appStack[key].layer.contextLayers, () => {
			g_appStack[key].layer.update();
		});
	}
}
// mod -s 20181031 motoda test

/**
 * 乗換案内に属するPOIである場合はtrueを返す
 */
function isTransitPoi(categoryName) {
	return ('乗換案内' == categoryName);
}
function clickedInfoWindowForWeb(poicode, url, categoryName) {
	console.log('clickedInfoWindowForWeb:ok!');
	console.log(poicode,url);
//	outputLog(PARAMATOR.Info, PARAMATOR.UserAction, 'clickPoiInfoWindow', {'poicode': poicode, 'url': url});
//	// openInAppBrowser(url);
//	evaluatePoi.setTargetPoicode(poicode);
	displayInContentView(url, categoryName);
//	window.open(url);
//	//ポップアップを閉じる
//	var closer = $('#popup-closer')[0];
//	closer.onclick();
}
/**
 * 指定されたurlをコンテンツビューに表示する
 */
function displayInContentView(url, categoryName){
	console.log('displayInContentView:ok!');
	// カテゴリに応じたPOI閲覧ビューを更新する
	changepoiviewer(url, categoryName);
	// カテゴリに応じたPOI閲覧ビューに切り替え
	//switchToPoiViewer(categoryNames);
}
/**
 * カテゴリに応じたPOI閲覧ビューを更新する
 */
function changepoiviewer(url, categoryName) {
	console.log('changepoiviewer:ok!');
	// 乗換案内の場合は乗換案内ビューを、それ以外の場合はコンテンツビューの内容を更新
	if (isTransitPoi(categoryName)) {
		//updateTransitView(url);
		$('#transit-frame').attr('src',url);
		switchTab('transit');
	} else {
		//updateContentView(url);
		$('#content-frame').attr('src',url);
		switchTab('content');
	}
}

//var tabid = ["map-view","content","event","transit"];
var tabid = ["map-view","event","transit"];
$(function() {
	$('.tab li').click(function() {
		var index = $('.tab li').index(this);


//		$('.tab li').removeClass('active');

//		$(this).addClass('active');

		switchTab(tabid[index]);
	});
});
/**
 * タブを切り替え
 */
function switchTab(domId, destname){
	console.log('switchTab:ok!',domId);
	$('.tab li').removeClass('active');
	var index = tabid.indexOf( domId );
	if(index>=0){
		$('.tab li').eq(index).addClass('active');
	}

	$('.tabs').hide(); //今表示しているタブを消す
	$('#' + domId).show(); //選択されたDOMを表示する
	// ヘッダのタイトルを変更
	var title = $('#' + domId).attr('data-title');
	$('#headerTitle').html(title);

	if(domId === "map-view"){
		$('#applicationInfo').show();
		$('#search').show();
	}
	else{
		$('#applicationInfo').hide();
		$('#search').hide();
	}

	//イベント情報の場合、要素を取得して表示する
	if(domId === "event"){
		myEventListUI.showOfficialEventList(destname);
		myEventListUI.showPersonalEventList(destname);
		if($("#official_event_list li").length === 0) {
			$("#official_event").hide();
			$("#personal_event").show();
		}

		if($("#personal_event_list li").length === 0) {
			$("#personal_event").hide();
			$("#official_event").show();
		}
	}
}
function viewDestInf(destname, event_count){
	if(event_count > 0){
		switchTab("event", destname);
	}
}
function searchText(){
	let stext = $('#searchBox').val();
//	let fromTime = $('#fromTime').timepicker('getTime');
//	let toTime =   $('#toTime').timepicker('getTime');
	let fromTime = $('#fromTime').val();
	let toTime =   $('#toTime').val();
	console.log(fromTime + " to " + toTime);
	if(!stext) return ;
//	alert(stext);
//選択中のアプリケーション
//	let app = $('#applicationInfo').text();
//	alert(app);

	// 検索条件セット
	var conditions = { search: { text: stext } };

/*
	//検索条件に表示条件を含める方式
	// コンテキストフィーダ側で表示条件判定のロジックが必要
	conditions.search.layerVisible = true;	// 表示中のレイヤーのみを対象とする
	//コンテキスト取得
	var context = gContextFeederStack.GetContext(conditions);
*/
	// コンテキスト取得対象をフィルタリングする方式
	var target = [];
//	var appList = document.getElementById("sortable").children;
//	for(let app of appList) {
//		var appName = app.children[0].innerText;
//		var checkId = app.children[0].children[0].id;
	var appList = gContextFeederStack.getFeeder("AppList").GetContext(null);
	for(let app of appList) {
		var appName = app.Name;
		var checkId = app.checkId;
		//「アプリ選択」にてチェックが入っているアプリのみを対象する
		if($("#"+checkId).prop('checked')){
			target.push(appName);
		}
	}
	// フィルタを設定してコンテキスト取得
	var context = gContextFeederStack.GetContext(conditions, function(name){ return (target.indexOf(name)>=0);});

/* クリックイベント送信方式だと地図上に重なった別のマーカーがポップアップするケースがあるので没
	var hit = false;
	for(key in context){
		for(let poi of context[key]){
//			alert(poi.name);
			//最初にヒットしたpoiにクリックイベントを発行
			var geometry = new ol.geom.Point(convertCoordinate(poi.locations.latlng.lng, poi.locations.latlng.lat));
			var coordinate = geometry.getCoordinates();
			var pixel = g_map.getPixelFromCoordinate(coordinate);
			console.log("searchText:"+pixel);
//			pixel[0] = Math.round(pixel[0]);
//			pixel[1] = Math.round(pixel[1]);
			pixel[0]+=10;
			pixel[1]-=10;
			console.log("searchText:"+pixel);
			var evt = {pixel: pixel, type: 'click'};
			g_map.dispatchEvent(evt);
			hit = true;
			break;
		}
	}
	if(!hit){
		alert(stext+"は見つかりません");
	}
*/
	var hit = false;
	for(key in context){
		for(let poi of context[key]){
//			alert(poi.name);
			//最初にヒットしたpoiをポップアップ
			let feature = g_appStack[key].layer.getFeature(poi);
			showPopup(g_appStack[key].layer, feature);
			hit = true;
			break;
		}
	}
	if(!hit){
		alert(stext+"は見つかりません");
	}
}

// ポップアップ
function showPopup(stLayer, feature){
	var content = document.getElementById('popup-content');

	// アプリに適用されたコンテキストに応じて、ポップアップの内容を変更できるようにする。
	withLayers(stLayer.contextLayers, () => {
		content.innerHTML = stLayer.getPopup(feature);
	});

	var coordinate = feature.getGeometry().getCoordinates();
	g_map.getOverlayById('popoverlay').setPosition(coordinate);
}

// mod -e 20181031 motoda test

