/**
	混雑コンテキストレイヤー
*/

g_config.ctx_layer_congestion.refineClass(StBaseLayer, {
	// マーカーのスタイル更新をオーバーライドしてカスタマイズ
	update(){
		// アプリケーションリスト取得
		var appList = gContextFeederStack.getFeeder("AppList").GetContext(null);

		var source = this.markerLayer.getSource();
		var features = source.getFeatures();
		for(let feature of features){
			var textColor = "#0000ff";
			var text = feature.get("content");
			var pinImage = this.pinImage;

			// poiCodeを指定してコンテキストセットを取得
			var conditions = {};
			conditions.id = feature.get("poiCode");
			var context = gContextFeederStack.GetContext(conditions);
			// 混雑コンテキスト取得
			var congestion = context["Congestion"];
			// 混雑しているか？
			if(congestion && congestion["averageCongestion"] > 0){
				text+= "("+congestion["starsStr"]+")";
				textColor = congestion["rankColor"];
				if(congestion["averageCongestion"]>2){
					pinImage = "img/marker-red.png";
				}
				else if(congestion["averageCongestion"]>1){
					pinImage = "img/marker-yellow.png";
				}
				else{
					pinImage = "img/marker-green.png";
				}
				// 自アプリがアプリケーションリストの先頭の場合
				if(this.categoryName == appList[0].Name){
					// テキスト付スタイル(アイコンビッグ)
					feature.setStyle(featureStyleBigWithText);
				}
				else{
					// デフォルトスタイル(アイコンビッグ)
					feature.setStyle(featureStyleBig);
				}
			}
			else{
				// 自アプリがアプリケーションリストの先頭の場合
				if(this.categoryName == appList[0].Name){
					// テキスト付スタイル
					feature.setStyle(featureStyleWithText);
				}
				else{
					// デフォルトスタイル
					feature.setStyle(featureStyleDefault);
				}
			}
			// 混雑状況による可変項目を再セット
			feature.set("text", text);
			feature.set("textColor", textColor);
			feature.set("markerSrc", pinImage);
		}
		source.dispatchEvent('change');
	}
/*
	// レイヤーのFeaturesをオーバーライド
	setFeatures(poiLocs){
		var features1 = []
		poiLocs.forEach( elm => {
			var context = gContextFeederStack.GetContext(elm);
			var textColor = "#0000ff";
			var text = elm.name;
			var pinImage = this.pinImage;
			var congestion = context["Congestion"];
			if(congestion && congestion["averageCongestion"] > 0){
				text+= "("+congestion["starsStr"]+")";
//				textColor = "#008000";
				textColor = congestion["rankColor"];
				if(congestion["averageCongestion"]>2){
					pinImage = "img/marker-red.png";
				}
				else if(congestion["averageCongestion"]>1){
					pinImage = "img/marker-yellow.png";
				}
				else{
					pinImage = "img/marker-green.png";
				}
			}

			var feature = new ol.Feature({
				geometry: new ol.geom.Point(convertCoordinate(elm.locations.latlng.lng, elm.locations.latlng.lat)),
				poiCode: elm.id,
				content: elm.name,
				text: text,
				textColor: textColor,
				markerSrc: pinImage
			});
			features1.push(feature);
		});
		return features1;
	}
*/
});
