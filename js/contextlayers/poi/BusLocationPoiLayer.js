/**
	バス現在位置アプリ用コンテキストレイヤー
*/

g_config.ctx_layer_poi_buslocation.refineClass(StBaseApp, {

	// POI取得処理
	async SetPoiLocs(){
		var poi_list = [];
		var poi_list_org = await _request( 'https://kataribe.cloud:53101/api/v1/', 'post', 'buslocations/current/get/', {} );

		for (var poi_data of poi_list_org.body) {
			poi_list.push(
				{
					id: poi_data.bus_id,
                    name: poi_data.bus_name,
                    contents: {
                        images:[
                            {
                                original: ""
                            }
                        ],
                        article: {
                            web: "",
                            text: " 現在位置：" + poi_data.place
                        }
                    },
                    tags: {
                        wp_categories: [],
                        wp_tags: [],
                    },
					locations: {
						latlng: {
							lat: poi_data.location[0],
							lng: poi_data.location[1]
						}
                    },
                    evaluations: []
				}
			);
		}

		this.poiLocs = poi_list;
	}

});

// リクエストを発行
function _request(api_root, method_type, endpoint, data, callBack = null) {

    var method;
    var url = api_root + endpoint

    switch ( method_type ) {
        case 'get':
            method = superagent.get(url);
            break;
        case 'post':
            method = superagent.post(url);
            break;
        case 'put':
            method = superagent.put(url);
            break;
        case 'delete':
            method = superagent.delete(url);
            break;
    }

    return new Promise((resolve, reject) => {
        method
        .send(data || {})
        .end((err, res) => {
            if (err) {
                reject(err);
            } else {
                if (callBack) {
                    callBack(res);
                }
                resolve(res);
            }
        });
    });
}
