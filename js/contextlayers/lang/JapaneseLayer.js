/**
	日本語用コンテキストレイヤー
*/

g_config.ctx_layer_lang_ja.refineClass(StBaseLayer, {

	update(){
		var source = this.markerLayer.getSource();
		var features = source.getFeatures();

		for(let feature of features){
			var properties = feature.getProperties();
			feature.set("text", properties.ja_text);
			feature.set("content", properties.ja_text);
        }
		source.dispatchEvent('change');

		// アプリにセットされている別のコンテキストレイヤーで実装されているupdateメソッドを呼び出す
		proceed();
    }
});
