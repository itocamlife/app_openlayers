/**
	英語用コンテキストレイヤー
*/

g_config.ctx_layer_lang_en.refineClass(StBaseLayer, {

	update(){
		var source = this.markerLayer.getSource();
		var features = source.getFeatures();

		for(let feature of features){
			var properties = feature.getProperties();
			var tags = properties.tags;
			var title = properties.ja_text;

			for (var tag of tags) {
				if (tag.startsWith('lang:en:')) {
					title = tag.slice(8);
					break;
				}
			}
			feature.set("text", title);
			feature.set("content", title);
        }
		source.dispatchEvent('change');

		// アプリにセットされている別のコンテキストレイヤーで実装されているupdateメソッドを呼び出す
		proceed();
    }
});
