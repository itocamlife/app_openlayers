/**
	バス停アプリ用コンテキストレイヤー
*/

g_config.app_layer_busstation.refineClass(StBaseLayer, {

	// マーカークリック時のポップアップ情報
	getPopup(feature){
//		console.log(this.categoryName);
		var poiCode = feature.get('poiCode');
		var congestion = gContextFeederStack.getFeeder("Congestion").getCongestion(poiCode);
		var averageCongestion_trn = congestion["averageCongestion_trn"],
			starsStr_trn = congestion["starsStr_trn"],
			rankColor_trn = congestion["rankColor_trn"];

		// 乗換案内の場合は使いやすさの評価も表示
		var averageRankText_trn = "使いやすさ:" + averageCongestion_trn;
		averageRankText_trn += ' <span style="color:' + rankColor_trn + '">' + starsStr_trn + '</span> <br>';

		var popupContent = `
			${averageRankText_trn}
		`;
		return proceed(feature)+popupContent;
	}

});
