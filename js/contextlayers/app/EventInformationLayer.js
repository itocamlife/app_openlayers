/**
	イベント情報アプリ用コンテキストレイヤー
*/

g_config.app_layer_event.refineClass(StBaseApp, {
	SetPoiLocs() {
		// 伊都キャンパスの全POI取得
		var poiLocsAll = g_myPoiController.getPoiLocs();

		// イベント情報が存在するPOIを絞り込み
		var selectedPoiLocs = [];
		for(let obj of poiLocsAll) {
			var infoWindowObj= g_myPoiController.getInfoWindowKeys(obj.id),
				destName = infoWindowObj["dest_name"],
				eventCount = g_myEventController.countEvent(destName);
			if(eventCount > 0 ){
				obj.eventCount = eventCount;
				obj.destName = destName;
				selectedPoiLocs.push(obj);
			}
		}
		this.poiLocs = selectedPoiLocs;
	}
});

g_config.app_layer_event.refineClass(StBaseLayer, {
	init(app){
		proceed(app);
		this.app = app;
	},
	// マーカークリック時のポップアップ情報
	getPopup(feature){
//		console.log(this.categoryName);
		var poiCode = feature.get('poiCode');
		for(let obj of this.app.poiLocs) {
			if(obj.id == poiCode){
				var eventCount = obj.eventCount;
				var destName = obj.destName;
				break;
			}
		}
		if(eventCount){
			var popupContent = `
				イベント: <a href="#" class="poi_event" onclick="viewDestInf('${destName}', ${eventCount})">${eventCount}件</a>
			`;
//				イベント: <a class="poi_event">${eventCount}件</a>
//				イベント: <a class="poi_event" onclick="viewDestInf('${destName}', ${eventCount})">${eventCount}件</a>
		}
		return proceed(feature)+popupContent;
	}

});

