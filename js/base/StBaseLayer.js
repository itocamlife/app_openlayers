/**
	ベースレイヤークラス
*/
class StBaseLayer {

	// コンストラクタ
	// ※contextjsでのwithLayersでコンストラクタにはレイヤーが適用できない（？）ので、
	//  以下のinitメソッドを実行することでコンストラクタ代わりとする

	//  categoryName	:アプリケーション名
	//  pinImage		:pinイメージ
	init( app ){
		this.categoryName = app.name;
		this.checkId = null;
		this.pinImage = app.pinImage ? app.pinImage : "img/marker-red.png";
		this.visible = $(this.checkId).prop('checked');
		this.markerLayer = null;
		this.c_contextLayers = app.contextLayers;
	}

	// マーカーのスタイルを更新する
	update(){
		// アプリケーションリスト取得
		var appList = gContextFeederStack.getFeeder("AppList").GetContext(null);

		var source = this.markerLayer.getSource();
		var features = source.getFeatures();
		for(let feature of features){
			// 自アプリがアプリケーションリストの先頭の場合
			if(this.categoryName == appList[0].Name){
				// テキスト付スタイル
				feature.setStyle(featureStyleWithText);
			}
			else{
				// デフォルトスタイル
				feature.setStyle(featureStyleDefault);
			}
		}
		source.dispatchEvent('change');
	}

	// レイヤーのFeatures
	setFeatures(poiLocs){

		// Layerを作成
		var features = []
		poiLocs.forEach( elm => {
			var feature = new ol.Feature({
				geometry: new ol.geom.Point(convertCoordinate(elm.locations.latlng.lng, elm.locations.latlng.lat)),
				poiCode: elm.id,
				content: elm.name,
				text: elm.name,
				ja_text: elm.name,
				tags: elm.tags.wp_tags,
				textColor: "#000000",
				markerSrc: this.pinImage
			});
			features.push(feature);
		});

		return features;
	}

	create(poiLocs){
		this.poiLocs = poiLocs;

		// アプリケーションリスト取得
		var appList = gContextFeederStack.getFeeder("AppList").GetContext(null);
		for(let app of appList) {
			if(app.Name == this.categoryName){
				this.checkId = "#"+app.checkId;
				break;
			}
		}
/*
		// デフォルトのマーカースタイル
		var markerStyleDefault = function(feature, resolution) {
			var styles = [
				new ol.style.Style({
					image: new ol.style.Icon( {
						anchor: [0.5, 1],
						anchorXUnits: 'fraction',
						anchorYUnits: 'fraction',
						opacity: 0.75,
						src: feature.get('markerSrc')
					}),
//					text: new ol.style.Text({
//						fill: new ol.style.Fill({color: feature.get("textColor")}),
//						stroke: new ol.style.Stroke({color: "#ffffff", width: 2}),
//						scale: 1.2,
//						textAlign: "center",
//						textBaseline: "top",
//						offsetY: 0,
//						text: feature.get("text"),
//						font: "Courier New, monospace"
//					})
				})
			];
			return styles;
		};
*/
		// Layerを作成
		var myFeatures = this.setFeatures(poiLocs);

		// POIデータ を Source にセット
		var markerSource = new ol.source.Vector({
			features: myFeatures
		});

		// Source を Layer にセット
		var markerLayer = new ol.layer.Vector({
			source: markerSource,
//			style: markerStyleDefault
		});
		this.markerLayer = markerLayer;

		// レイヤー名セット
		markerLayer.set("name", this.categoryName);

		// 自分自身をセット
		markerLayer.set("StLayer", this);

		// レイヤー追加
		g_map.addLayer(markerLayer);

		withLayers(this.c_contextLayers, () => {
			this.update();
		});

//		var source = markerLayer.getSource();
//		var params = source.getParams();
//		params.t = new Date().getMilliseconds();
//		source.updateParams(params);


		// Layer表示切替
		$(document).on("change", this.checkId, () => {
			this.visible = $(this.checkId).prop('checked');
			if ( $(this.checkId).prop('checked') ) {
				markerLayer.setVisible(true);
			} else {
				markerLayer.setVisible(false);
			}
		});
	}

	// マーカークリック時のポップアップ情報
	getPopup(feature){
//		console.log(this.categoryName);
		var poiCode = feature.get('poiCode');
		var congestion = gContextFeederStack.getFeeder("Congestion").getCongestion(poiCode);
		var averageCongestion = congestion["averageCongestion"],
				starsStr = congestion["starsStr"],
				rankColor = congestion["rankColor"],
				infoWindowObj = g_myPoiController.getInfoWindowKeys(poiCode, this.poiLocs),
				url = infoWindowObj["url"],
				destName = infoWindowObj["dest_name"],
				text = infoWindowObj["text"],
				imageSrc = infoWindowObj["imageSrc"],
				tags = infoWindowObj["tags"];
//				eventCount = g_myEventController.countEvent(destName)

		var related_pois_html = '';
		var child_poi_ids = infoWindowObj['child_poi_ids'];

		if ( child_poi_ids.length ) {
			related_pois_html = '<br><br>関連施設';
			for (var child_poi_id of child_poi_ids ) {
				var child_poi_info = g_myPoiController.getInfoWindowKeys(child_poi_id, this.poiLocs);
				related_pois_html += `<div>
									<a href="#" onclick="clickedInfoWindowForWeb('${child_poi_id}', '${child_poi_info["url"]}', '${this.categoryName}')">
									${child_poi_info["dest_name"]}
									</a>`;
			}
		}
		var destNameLabel = "名称:" + destName,
				textLabel = "説明:" + text,
				tagLabel = "タグ:" + tags,
				averageRankLabel = "混雑度:" + averageCongestion;

		var popupContent = `
			<a href="#" id="${url}" onclick="clickedInfoWindowForWeb('${poiCode}', this.id, '${this.categoryName}');">
				<img id="poi-img" src="${imageSrc}" style="height:100px;"> <br>
				${destNameLabel} <br>
			</a>
			${textLabel} <br>
			${tagLabel} <br>
			${averageRankLabel} <span style="color:${rankColor}">${starsStr}</span> <br>
			${related_pois_html}
		`;
//			イベント: <a class="poi_event">${eventCount}件</a>
//				イベント: <a class="poi_event" onclick="viewDestInf('${destName}', ${eventCount})">${eventCount}件</a>
		return popupContent;
	}

	//レイヤーを返す
	get layer(){
		return this.markerLayer;
	}

	get contextLayers() {
		return this.c_contextLayers;
	}

	// poiのfeatureを返す
	getFeature(poi){
		var source = this.markerLayer.getSource();
		var features = source.getFeatures();
		for(let feature of features){
			if(feature.get('poiCode')==poi.id){
				return feature;
			}
		}
		return null;
	}
}

// デフォルトのマーカースタイル
var featureStyleDefault = function(resolution) {
	var styles = [
		new ol.style.Style({
			image: new ol.style.Icon( {	// @type {olx.style.IconOptions}
				anchor: [0.5, 1],
				anchorXUnits: 'fraction',
				anchorYUnits: 'fraction',
				opacity: 0.75,
				src: this.get('markerSrc')
			})
		})
	];
	return styles;
};

var featureStyleBig = function(resolution) {
	var styles = [
		new ol.style.Style({
			image: new ol.style.Icon( {	// @type {olx.style.IconOptions}
				anchor: [0.5, 1],
				anchorXUnits: 'fraction',
				anchorYUnits: 'fraction',
				opacity: 0.75,
				scale: 1.5,
				src: this.get('markerSrc')
			})
		})
	];
	return styles;
};


// テキスト有マーカーのスタイル
var featureStyleWithText = function(resolution) {
	var styles = [
		new ol.style.Style({
			image: new ol.style.Icon( { // @type {olx.style.IconOptions}
				anchor: [0.5, 1],
				anchorXUnits: 'fraction',
				anchorYUnits: 'fraction',
				opacity: 0.75,
				src: this.get('markerSrc')
			}),
			text: new ol.style.Text({
				fill: new ol.style.Fill({color: this.get("textColor")}),
				stroke: new ol.style.Stroke({color: "#ffffff", width: 2}),
				scale: 1.2,
				textAlign: "center",
				textBaseline: "top",
				offsetY: 0,
				text: this.get("text"),
				font: "Courier New, monospace"
			})
		})
	];
	return styles;
};

var featureStyleBigWithText = function(resolution) {
	var styles = [
		new ol.style.Style({
			image: new ol.style.Icon( { // @type {olx.style.IconOptions}
				anchor: [0.5, 1],
				anchorXUnits: 'fraction',
				anchorYUnits: 'fraction',
				opacity: 0.75,
				scale: 1.5,
				src: this.get('markerSrc')
			}),
			text: new ol.style.Text({
				fill: new ol.style.Fill({color: this.get("textColor")}),
				stroke: new ol.style.Stroke({color: "#ffffff", width: 2}),
				scale: 1.2,
				textAlign: "center",
				textBaseline: "top",
				offsetY: 0,
				text: this.get("text"),
				font: "Courier New, monospace"
			})
		})
	];
	return styles;
};
