class StBaseApp {
	// コンストラクタ
	// name: アプリケーション名(POIカテゴリ名)
	// pinImage: ピンアイコン用画像ファイルパス
	// contextLayers: クラスに適用するコンテキストレイヤーのリスト
	init(name, pinImage, contextLayers) {
		this.name = name;
		this.pinImage = pinImage;
		this.contextLayers = contextLayers;

		withLayers(this.contextLayers, async () => {
			await this.SetPoiLocs();

			//このアプリケーションをコンテキストフィーダースタックに追加
			gContextFeederStack.AddContextFeeder(this);

			this.createAppLayer();
		});
	}

	// アプリレイヤーの生成
	createAppLayer() {
		//レイヤー作成
		this.layer = new StBaseLayer();

		// アプリに適用されたコンテキストに応じて、アプリレイヤーの初期処理を変更可能とする。
		withLayers(this.contextLayers, () => {
			this.layer.init(this);
		});

		this.layer.create(this.GetPoiLocs());
	}

	removeContextLayer(context_layer) {
		this.contextLayers = this.contextLayers.filter(obj => {
			return obj !== context_layer;
		});
		this.layer.c_contextLayers = this.contextLayers;
	}

	addContextLayer(context_layer, insert_bottom=true) {
		if (insert_bottom) {
			this.contextLayers.push(context_layer);
		} else {
			this.contextLayers.unshift(context_layer);
		}
		this.layer.c_contextLayers = this.contextLayers;
	}

	// 伊都キャンパスの全POI取得
	async SetPoiLocs() {
		var poiLocsAll = g_myPoiController.getPoiLocs();
		var categoryNames = [this.name];
		//カテゴリ名で絞り込んでこのアプリケーションのPOIリストとする
		this.poiLocs = g_myPoiController.choosePoiByCategory(poiLocsAll, categoryNames);
	}

	// アプリケーションのPOIリストを返す
	GetPoiLocs() {
		return this.poiLocs;
	}

	//コンテキスト取得
	// conditions 条件
	//  未指定：このアプリのPOIリストを返す
	//  conditions.search.text : テキスト検索条件をセット
	// 戻り値:poiリスト
	GetContext(conditions){
		//
		var context = [];
//		// アプリケーションがレイヤーを持たないor非表示の場合はnullを返す
//		if(!this.layer || !this.layer.visible) return null;

		// 検索条件未指定の場合
		if(!conditions || !conditions.search) {
			// アプリケーションのPOIリストを返す
			return this.poiLocs;
		}
		// 検索条件にレイヤー表示条件がある場合
		if(conditions.search.layerVisible && conditions.search.layerVisible!=this.layer.visible) return null;

		//テキスト検索
		if(conditions.search.text){
			let stext = conditions.search.text;
			for ( let poi of this.poiLocs ) {
				// name、tag、descriptionで検索して絞り込み
				if(	poi.name.indexOf(stext)>=0 ||
					poi.contents.article.text.indexOf(stext)>=0 ||
					poi.tags.wp_tags.join(",").indexOf(stext)>=0){
					context.push(poi);
				}
			}
		}
		return context.length>0 ? context : null;
	}
}
